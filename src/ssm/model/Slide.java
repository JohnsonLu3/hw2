package ssm.model;

/**
 * This class represents a single slide in a slide show.
 *
 * @author McKilla Gorilla & _____________
 */
public class Slide {

    String imageFileName;
    String imagePath;
    String imageCaption;

    /**
     * Constructor, it initializes all slide data.
     *
     * @param initImageFileName File name of the image.
     *
     * @param initImagePath File path for the image.
     *
     */
    public Slide(String initImageFileName, String initImagePath, String initCaption) {
        imageFileName = initImageFileName;
        imagePath = initImagePath;
        imageCaption = initCaption;
    }

    // ACCESSOR METHODS
    public String getImageFileName() {
        return imageFileName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getCaption() {
        return imageCaption;
    }

    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
        imageFileName = initImageFileName;
    }

    public void setImagePath(String initImagePath) {
        imagePath = initImagePath;
    }

    public void setImage(String initPath, String initFileName) {
        imagePath = initPath;
        imageFileName = initFileName;
    }
    public void setCaption(String initCaption){
        imageCaption = initCaption;
    }
    
    public void setAll(Slide sl){
        setCaption(sl.getCaption());
        setImageFileName(sl.getImageFileName());
        setImagePath(sl.getImagePath());
    }

}
