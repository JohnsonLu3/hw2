package ssm.view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_TITLE;
import static ssm.LanguagePropertyType.FILE_NOT_FOUND;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.file.SlideShowFileManager;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.error.ErrorHandler;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    //HBox selectedSlide;
    SlideShowMakerView SSMV = new SlideShowMakerView(new SlideShowFileManager());
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    public TextField captionTextField;
    public static String caption;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    public void setCaption(){
        caption = captionTextField.getText();
    }
    public String getCaption(){
        return captionTextField.getText();
    }
    
    public static String getSlideCaption(){
        return caption;
    }
    public Slide getSlide(){
        return slide;
    }

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
	captionTextField = new TextField();
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
       setCaption();
       captionTextField.setText(slide.getCaption());
       
 


	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
	    imageController.processSelectImage(slide, this);
           SlideShowMakerView.getFileController().markAsEdited();
	});

       captionTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("textfield changed from " + oldValue + " to " + newValue);
            SlideShowMakerView.getFileController().markAsEdited();
            slide.setCaption(getCaption());
        });
       /**
        * CHANGE THE SELECTED SLIDE COLOR, IF THERE WAS A PREVIOUSLY
        * SELECTED SLIDE THEN CHANGE THE COLOR OF IT BACK TO THE DEFUALT
        */
       setOnMousePressed(e->{
           if(SelectedSlide.getSelected() != null){
               SelectedSlide.getSelected().setStyle("-fx-background-color:  #E53935");
           }
           SelectedSlide.setSelected(this);
           SelectedSlide.setSelectedSlide(getSlide());
           SelectedSlide.setColor();
           boolean enable = true;
       });
        

       
    }
  
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();

	try {
           File file = new File(imagePath);
           if(!file.exists()){
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    String file_404_TITLE = props.getProperty(ERROR_TITLE.toString());
                    String file_404 = props.getProperty(FILE_NOT_FOUND.toString());
                    Alert openError = new Alert(AlertType.ERROR);
                    openError.setTitle(file_404_TITLE);
                    openError.setHeaderText(file_404);
                    openError.setContentText(imagePath);

                    openError.showAndWait();
           }else{
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
           }
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image

        }
    }
    

}