/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import javafx.scene.layout.HBox;
import ssm.model.Slide;
import ssm.model.SlideList;


/**
 *
 * @author JL
 */
public class SelectedSlide {
    public static HBox selectedHBox;
    public static Slide selectedSlide;
    public static boolean isSelected;
    public static int pos;
    
    
    /**
     * Sets Selected Hbox
     * @param slSlide 
     */
    public static void setSelected(HBox slSlide){
        selectedHBox = slSlide;
        isSelected = true;
    }
    
    public static void setColor(){
        getSelected().setStyle("-fx-background-color: #FFCF53");
    }
    
    /**
     * 
     * @return Selected HBox
     */
    public static HBox getSelected(){
        return selectedHBox;
    }
    
    /**
     * Sets selected Slide
     * @param slide 
     */
    public static void setSelectedSlide(Slide slide){
        selectedSlide = slide;
        pos = (SlideList.getList().indexOf(selectedSlide));
        enableUpButton();
        enableDownButton();
        enableRemoveButton(true);
        isSelected = true;
    }
    
    /**
     * 
     * @return return selectedSlide
     */
    public static Slide getSelectedSlide(){
        return selectedSlide;
    }
    
    /**
     * 
     * @return isSelected 
     */
    public static boolean isSelected(){
        return isSelected;
    }
      
    /**
     * Deselect Selected Slide
     * @return true if Selected Slide was deselected
     */
    public static boolean  deselectSlide(){
        setSelected(null);
        setSelectedSlide(null);
        enableRemoveButton(false);
        disableMoveButton();
        isSelected = false;
        return true;
    }
    
    /**
     * Enable Up Button
     * if the selected slide is on the top then disabel
     * else enable
     */
    public static void enableUpButton(){
        if(isSelected == true){
             if(pos == 0){
                //DISABLE UP BUTTON
                   MoveButtonsView.getUpButt().setDisable(true);
                }else{
                //ENABLE UP BUTTON
                   MoveButtonsView.getUpButt().setDisable(false); 
                }
        }
    }
    
    /**
     * Enable the Down Button
     * if it selected slide is on the bottom then disable
     * else enable it.
     */
    public static void enableDownButton(){
        if(isSelected == true){
              if(pos == (SlideList.getList().size() -1)){
                    //DISABLE DOWN BUTTON
                        MoveButtonsView.getDownButt().setDisable(true);
                    }else{
                    //ENABLE DOWN BUTTON
                        MoveButtonsView.getDownButt().setDisable(false);
                    }
        }
        
    }
    
    public static void disableMoveButton(){
        MoveButtonsView.getDownButt().setDisable(true);
        MoveButtonsView.getUpButt().setDisable(true);
    }
    
    public static void enableRemoveButton(boolean enable){
        MoveButtonsView.getRemoveButt().setDisable(!enable);
    }

}
