/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.FILE_NOT_FOUND;
import static ssm.LanguagePropertyType.FILE_NOT_FOUND_TITLE;
import ssm.model.Slide;
import ssm.model.SlideList;
import ssm.model.SlideShowModel;

/**
 *
 * @author JL
 */
public class SlideShowViewer {
    
    public ImageView imgView = new ImageView();
    public Image img;
    public Stage viewer;
    public String cap;
    public VBox slidesView;
    public ObservableList<Slide> slidesToShow = SlideList.getList();
    public Button next, prev;
    public FlowPane buttonPane;
    public Label caption = new Label(cap);
    public SlideShowModel slideShowM;
    private int index = 0;
    private int listSize = slidesToShow.size();
   
    
    public int getIndex(){
        return index;
    }
    
    SlideShowViewer(Button nextButton, Button prevButton, FlowPane initiButtonPane, SlideShowModel SSM){
        next = nextButton;
        prev = prevButton;
        buttonPane = initiButtonPane;
        slideShowM = SSM;
        initView();
    }
    
    public void view(){
        cap = getCaption(index);
        String imgPath = getImagePath(index) + "/" + getImageName(index);
        File file = new File(imgPath);
        try{
            URL fileURL = file.toURI().toURL();
            img = new Image(fileURL.toExternalForm());
            imgView.setImage(img);
            caption.setText(cap);
            
            // AND RESIZE IT
	    //double scaledWidth = 650;
	    //double perc = scaledWidth / img.getWidth();
	    //double scaledHeight = img.getHeight() / perc;
	    imgView.setFitWidth(720);
	    imgView.setFitHeight(480);
        }catch(Exception e){
            //@todo - error handler for missing image
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String FILE_404_TITLE = props.getProperty(FILE_NOT_FOUND_TITLE.toString());
            String FILE_404 = props.getProperty(FILE_NOT_FOUND.toString());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(FILE_404_TITLE);
            alert.setContentText(FILE_404);
            
            alert.showAndWait();
        }
        
    }
    
    public String getCaption(int i){
        return slidesToShow.get(i).getCaption();
    }
    
    public String getImagePath(int i){
        return slidesToShow.get(i).getImagePath();
    }
    
    public String getImageName(int i){
        return slidesToShow.get(i).getImageFileName();
    }
    
    public void initView(){
        Stage viewStage = new Stage();
        viewStage.setTitle(slideShowM.getTitle());
        //Label showTitle = new Label(LABEL_SLIDE_SHOW_TITLE);

        StackPane top = new StackPane();
        StackPane middle = new StackPane();
        StackPane bottom = new StackPane();
        StackPane buttons = new StackPane();
        
        view();
        
        //top.getChildren().add(showTitle);
        middle.getChildren().add(imgView);
        bottom.getChildren().add(caption);
        buttons.getChildren().add(buttonPane);
        
        
        viewStage.setWidth(950);
        viewStage.setHeight(950);
        
        
        
        VBox viewBox = new VBox();
        //viewBox.getChildren().add(top);
        viewBox.getChildren().add(middle);
        viewBox.getChildren().add(bottom);
        viewBox.getChildren().add(buttons);
        Scene viewScene = new Scene(viewBox);
        viewStage.setScene(viewScene);
        viewStage.show();
        next.setDisable(false);
        prev.setDisable(false);
        
        next.setOnAction(e->{
            if(index == listSize-1){
                index = 0;
                view();
            }else{
                index++;
                view();
            }
        });
        
        prev.setOnAction(e->{
            if(index == 0){
                index = listSize-1;
                view();
            }else{
                index--;
                view();
            }
        });
    }

}
